// ==UserScript==
// @name         MusicBrainz: open all entities on collection page
// @description  Adds a button to open all entities on a collection page in a new tab
// @license      CC0
// @version      0.2
// @author       RandomMushroom128
// @grant        none
// @match        *://musicbrainz.org/collection/*
// @match        *://beta.musicbrainz.org/collection/*
// @match        *://test.musicbrainz.org/collection/*
// ==/UserScript==
// This doesn't work well with area collections9
const collection_type = document.querySelector("#content > h2").innerHTML.replace(/\s+/g, '-').replace(/.$/, '').toLowerCase()
const collection_entities = document.querySelectorAll(`table > tbody > tr > td > a[href^='/${collection_type}']:not([href$='art'])`)
const tabs = document.querySelector("ul.tabs")
addButton();
function addButton() {
    let button= document.createElement("button");
    tabs.appendChild(button);

    button.innerHTML = 'Open all entities in new tabs';
    button.style.cursor = 'pointer';
    button.addEventListener('click', function(){
        for (const entity of collection_entities) {
            window.open(entity.href, "_blank");
        }});
}